<?php

require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

//release 0

$sheep = new animal("shaun");

echo "Animal name: ".$sheep->name."<br>"; // "shaun"
echo "Legs: ".$sheep->legs."<br>"; // 4
echo "Cold blooded: ".$sheep->cold_blooded."<br><br>"; // "no"

//release 1

$sungokong = new ape("kera sakti");

$kodok = new frog("buduk");

echo "Animal name: ".$kodok->name."<br>"; // "shaun"
echo "Legs: ".$kodok->legs."<br>"; // 4
echo "Cold blooded: ".$sheep->cold_blooded."<br>"; // "no"
echo $kodok->jump()."<br><br>"; // "no"

echo "Animal name: ".$sungokong->name."<br>"; // "shaun"
echo "Legs: ".$sungokong->legs."<br>"; // 4
echo "Cold blooded: ".$sheep->cold_blooded."<br>"; // "no"
echo $sungokong->yell(); // "no"

?>